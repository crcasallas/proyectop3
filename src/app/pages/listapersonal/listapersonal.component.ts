import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Personaje, RPersonaje } from 'src/app/interface/personaje.interface';
import { PersonajeService } from 'src/app/services/personaje.service';
import { ModalComponent } from '../modal/modal.component'; 

@Component({
  selector: 'app-listapersonal',
  templateUrl: './listapersonal.component.html',
  styleUrls: ['./listapersonal.component.scss']
})
export class ListapersonalComponent implements OnInit {

  displayedColumns: string[] = ['id', 'img', 'name', 'editar', 'eliminar']; 
  //dataSource = new MatTableDataSource<any>([]);
  dataSource!:MatTableDataSource<Personaje>;
  // myCart$!:Observable<any>;
  myCart$:any;
  
  indice!:number;

  @ViewChild('paginator') paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  ngAfterViewInit() {    
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator; 
  }

  constructor(public personajeService: PersonajeService, public dialog: MatDialog) { } 

  ngOnInit(): void {    
    this.obtenerPersonaje();
  }

  obtenerPersonaje(){
    this.myCart$=this.personajeService.myCart$.subscribe((resp:any)=>{
      this.dataSource = new MatTableDataSource(resp.slice());
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator
      const res: any= [];
      Object.keys(resp).forEach( key=>{
        const person: any= resp[key];
        person.idel = key;
        res.push(person);
      })
      return res;        
    })
  }

  openDialog(i:number) {
    this.dialog.open(ModalComponent,{
      height: '35%',
      width: '90%',
    });
    this.personajeService.verPersonaje(i)      
  }

  eliminarDeLalista(i:number){         
    this.personajeService.eliminarDeLista(i);
  }

  applyFilterLP($event:any){
    const filterValue =($event.target as HTMLInputElement).value;    
    this.dataSource.filter = filterValue.trim();
    if (this.dataSource.paginator) {
       this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filter===''){
      this.obtenerPersonaje();
     }else{
      this.myCart$=this.personajeService.myCart$.subscribe(resp=>{
        resp;
      }) 
    }    
  } 

  ngOnDestroy(): void{
    this.myCart$.unsubscribe();
  }
}
