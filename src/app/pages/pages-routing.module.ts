import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { ListageneralComponent } from './listageneral/listageneral.component';
import { ListagridComponent } from './listagrid/listagrid.component';
import { ListapersonalComponent } from './listapersonal/listapersonal.component';
import { PagesComponent } from './pages.component';

const routes: Routes = [
    { path:'pages', component:PagesComponent, canActivate:[AuthGuard],
      children: [          
          { path:'listageneral', component:ListageneralComponent },
          { path:'listapersonal', component:ListapersonalComponent },
          { path:'listagrid', component:ListagridComponent },
          { path:'**', redirectTo:'listageneral', pathMatch:'full'}
      ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
