import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { RouterModule } from '@angular/router';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { ListageneralComponent } from './listageneral/listageneral.component';
import { ListapersonalComponent } from './listapersonal/listapersonal.component';
import { ModalComponent } from './modal/modal.component';
import { ListagridComponent } from './listagrid/listagrid.component';

@NgModule({
  declarations: [
    PagesComponent,
    ListageneralComponent,
    ListapersonalComponent,
    ModalComponent,
    ListagridComponent
  ],
  exports: [
    PagesComponent,
    ListageneralComponent,
    ListapersonalComponent,
    ModalComponent
  ],
  imports: [
    PagesRoutingModule,
    CommonModule,
    MaterialModule
  ]
})
export class PagesModule { }
