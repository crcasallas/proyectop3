import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { PagesRoutingModule } from './pages/pages-routing.module';

const routes: Routes = [
  { path:'login', component:LoginComponent },
  { path:'', redirectTo:'/login', pathMatch:'full'},
  { path:'pages', loadChildren: () => import('./pages/pages.module').then(m=>m.PagesModule)}//se supone que realiza el lazy loading pero no funciona de momento
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash:true}), 
            PagesRoutingModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
